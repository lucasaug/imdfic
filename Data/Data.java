/*
Crie uma classe para representar datas.

1. Represente uma data usando três atributos: o dia, o mês, e o ano.
2. Sua classe deve ter um construtor que inicializa os três atributos e verifica a validade dos valores fornecidos.
3. Forneça um construtor sem parâmetros que inicializa a data com a data atual fornecida pelo sistema operacional.
4. Forneça um método set um get para cada atributo.
5. Forneça o método toString para retornar uma representação da ata como string. Considere que a data deve ser formatada mostrando o dia, o mês e o ano separados por barra (/).
6. Forneça uma operação para avançar uma data para o dia seguinte.
*/

import java.lang.Math;

public class Data {
    private final short MILISEGUNDOS_POR_SEGUNDO = 1000;
    private final byte SEGUNDOS_POR_MINUTO = 60;
    private final byte MINUTOS_POR_HORA = 60;
    private final byte HORAS_POR_DIA = 24;
    private final byte MESES_POR_ANO = 12;
    private final float DIAS_POR_ANO = (float)365.25;
    private final float DIAS_POR_MÊS = DIAS_POR_ANO / MESES_POR_ANO;
    private final short INÍCIO_DA_ÉPOCA = 1970;
    private final byte PRIMEIRO_MÊS = 1;
    private final byte ÚLTIMO_MÊS = 12;
    private final byte PRIMEIRO_DIA = 1;
    private final byte ÚLTIMO_DIA = 31;
    private final byte[] MESES_BISSEXTO = { 31, 29, 31, 30, 31, 30,
                                            31, 31, 30, 31, 30, 31 };
    private final byte[] MESES_NÃO_BISSEXTO = { 31, 28, 31, 30, 31, 30,
                                                31, 31, 30, 31, 30, 31 };

    private short ano;
    private byte mês;
    private byte dia;

    public Data() {
        long milisegundos = System.currentTimeMillis();
        double segundos = milisegundos / MILISEGUNDOS_POR_SEGUNDO;
        double minutos = segundos / SEGUNDOS_POR_MINUTO;
        double horas = minutos / MINUTOS_POR_HORA;
        double dias = horas / HORAS_POR_DIA;

        this.ano = (short)(INÍCIO_DA_ÉPOCA + Math.floor(dias / DIAS_POR_ANO));
        short meses_até_fim_ano_anterior = (short)(MESES_POR_ANO * (ano - INÍCIO_DA_ÉPOCA));
        this.mês = (byte)(Math.ceil((dias / (DIAS_POR_MÊS))) - meses_até_fim_ano_anterior);
        this.dia = (byte)Math.ceil(dias - ((meses_até_fim_ano_anterior + mês - 1) * DIAS_POR_MÊS));
    }
    public Data(byte dia, byte mês, short ano) {
        if (verificaData(dia, mês, ano) == true) {
            this.dia = dia;
            this.mês = mês;
            this.ano = ano;
        } else {
            this.dia = PRIMEIRO_DIA;
            this.mês = PRIMEIRO_MÊS;
            this.ano = INÍCIO_DA_ÉPOCA;
            System.out.println("Data configurada para o dia " + this.toString() + ".");
        }
    }
    private boolean éBissexto(short ano) {
        return ((ano % 4 == 0 && ano % 100 != 0) || ano % 400 == 0);
    }
    private boolean verificaDia(byte dia, byte mês, short ano) {
        if (dia < PRIMEIRO_DIA || dia > ÚLTIMO_DIA) {
            System.out.println("Erro, dia inválido.");
            return false;
        } else if (dia == 29 && mês == 2 && éBissexto(ano) == false) {
            System.out.println("Erro, dia 29 de fevereiro em ano não bissexto.");
            return false;
        } else if (éBissexto(ano) == true && MESES_BISSEXTO[mês - 1] < dia || éBissexto(ano) == false && MESES_NÃO_BISSEXTO[mês - 1] < dia) {
            System.out.println("Erro, dias insuficientes para o mês " + mês + ".");
            return false;
        }
        return true;
    }
    private boolean verificaMês(byte mês) {
        if (mês < PRIMEIRO_MÊS || mês > ÚLTIMO_MÊS) {
            System.out.println("Erro, mês inválido");
            return false;
        }
        return true;
    }
    private boolean verificaData(byte dia, byte mês, short ano) {
        if (verificaDia(dia, mês, ano) == false) {
            return false;
        } else if (verificaMês(mês) == false) {
            return false;
        }
        return true;
    }
    public void setDia(byte dia) {
        if (verificaDia(dia, this.mês, this.ano) == true) {
            this.dia = dia;
        }
    }
    public byte getDia() {
        return this.dia;
    }
    public void setMês(byte mês) {
        if (verificaMês(mês) == true) {
            this.mês = mês;
        }
    }
    public byte getMês() {
        return this.mês;
    }
    public void setAno(short ano) {
        if (this.mês == 2 && this.dia == 29 && éBissexto(ano) == false) {
            this.mês++;
            this.dia = PRIMEIRO_DIA;
        }
        this.ano = ano;
    }
    public short getAno() {
        return this.ano;
    }
    public String toString() {
        return new String(this.dia + "/" + this.mês + "/" + this.ano);
    }
    public void próximoDia() {
        if (this.dia > 27) {
            if (this.mês == ÚLTIMO_MÊS && this.dia == MESES_NÃO_BISSEXTO[this.mês - 1]) {
                this.dia = PRIMEIRO_DIA;
                this.mês = PRIMEIRO_MÊS;
                this.ano++;
                return;
            } else if (this.mês == 2 && (MESES_BISSEXTO[this.mês - 1] == this.dia || MESES_NÃO_BISSEXTO[this.mês - 1] == this.dia)) {
                this.dia = PRIMEIRO_DIA;
                this.mês++;
                return;
            } else if (MESES_NÃO_BISSEXTO[this.mês - 1] == this.dia) {
                this.dia = PRIMEIRO_DIA;
                this.mês++;
                return;
            }
        }
        this.dia++;
    }
}

