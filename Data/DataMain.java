public class DataMain {
    public static void main(String[] args) {
        Data data = new Data();
        System.out.println(data.toString());

        Data fimDeAno = new Data((byte)31, (byte)12, (short)1989);
        System.out.println(fimDeAno.toString());
        fimDeAno.próximoDia();
        System.out.println(fimDeAno.toString());

        Data bissexto = new Data((byte)29, (byte)2, (short)1990);
        System.out.println(bissexto.toString());
        bissexto.setAno((short)1992);
        bissexto.setMês((byte)2);
        bissexto.setDia((byte)29);
        System.out.println(bissexto.toString());
        bissexto.próximoDia();
        System.out.println(bissexto.toString());

        Data nãoBissexto = new Data((byte)28, (byte)2, (short)1990);
        System.out.println(nãoBissexto.toString());
        nãoBissexto.próximoDia();
        System.out.println(nãoBissexto.toString());
    }
}
